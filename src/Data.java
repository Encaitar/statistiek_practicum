public class Data 
{
    public int[] inputNeurons;
    public double[][] inputRelations;
    public double[][] inputRelationsLastDelta;
    
    public double[] hiddenNeurons;
    public double[] errorHiddenNeurons;
    
    public double[] output;
    public double[] errorOutput;
    
    public double[][] hiddenRelations;
    public double[][] hiddenRelationsLastDelta;
    
    public Data(int[][] firstImage, int lengthHidden, int lengthOutput)
    {
        inputNeurons = new int[getImageLength(firstImage)];
        inputRelations = new double[lengthHidden][getImageLength(firstImage)];
        inputRelationsLastDelta = new double[lengthHidden][getImageLength(firstImage)];
        hiddenNeurons = new double[lengthHidden];
        errorHiddenNeurons = new double[lengthHidden];
        hiddenRelations = new double[lengthOutput][lengthHidden]; //kolommen == length hidden, rijen = length output
        hiddenRelationsLastDelta = new double[lengthOutput][lengthHidden];
        
        output = new double[lengthOutput];
        errorOutput = new double[lengthOutput];
        
        inputRelations = Utils.fillRandommised(inputRelations);
        hiddenRelations = Utils.fillRandommised(hiddenRelations);
    }
    
    public double[] getHiddenNeurons() {
        return hiddenNeurons;
    }

    public void setHiddenNeurons(double[] hiddenNeurons) {
        this.hiddenNeurons = hiddenNeurons;
    }
    public void hiddenNeuronsToSigmoid() {
        double sigmoids[] = new double[this.hiddenNeurons.length];
        for (int i = 0; i < sigmoids.length; i++) {
            sigmoids[i] = Utils.sigMoid(this.hiddenNeurons[i]);
        }
        setHiddenNeurons(sigmoids);
    }
    public void setHiddenNeuron(int position, double waarde) {
        this.hiddenNeurons[position] = waarde;
    }

    public double[] getErrorHiddenNeurons() {
        return errorHiddenNeurons;
    }
    public void setErrorHiddenNeurons(double[] errorHiddenNeurons) {
        this.errorHiddenNeurons = errorHiddenNeurons;
    }
    public void setErrorHiddenNeuron(int position, double waarde) {
        this.errorHiddenNeurons[position] = waarde;
    }    
    
    public int[] getInputNeurons() {
        return inputNeurons;
    }

    public void setInputNeurons(int[] inputNeurons) {
        this.inputNeurons = inputNeurons;
    }

    public double[] getErrorOutput() {
        return errorOutput;
    }

    public void setErrorOutput(double[] errorOutput) {
        this.errorOutput = errorOutput;
    }
    
    public void setErrorOutput(int position, double waarde) {
        this.errorOutput[position] = waarde;
    }

    public double[] getOutput() {
        return output;
    }
    public int[] getOutputInt() {
        int[] outputInt = new int[output.length];
        for (int i = 0; i < output.length; i++) {
            if(output[i] >= 0.5)
            {
                outputInt[i] = 1;
            }
            else
            {
                outputInt[i] = 0;
            }
        }
        return outputInt;
    }

    public void setOutput(double[] output) {
        this.output = output;
    }
    
    public void setOutput(int position, double waarde) {
        this.output[position] = waarde;
    }
          
    private int getImageLength(int[][] firstImage) {
        int count = 0;
        for (int i = 0; i < firstImage.length; i++) {
            count += firstImage[i].length;
        }
        return count;
    }

    public double[][] getInputRelations() {
        return inputRelations;
    }

    public void setInputRelations(double[][] inputRelations) {
        this.inputRelations = inputRelations;
    }
    
    public void setInputRelations(int positionRij, int positionKolom, double waarde) {
        this.inputRelations[positionRij][positionKolom] = waarde;
    }

    public double[][] getInputRelationsLastDelta() {
        return inputRelationsLastDelta;
    }

    public void setInputRelationsLastDelta(double[][] inputRelationsLastDelta) {
        this.inputRelationsLastDelta = inputRelationsLastDelta;
    }
    
    public void setInputRelationsLastDelta(int positionRij, int positionKolom, double waarde) {
        this.inputRelationsLastDelta[positionRij][positionKolom] = waarde;
    }

    public double[][] getHiddenRelations() {
        return hiddenRelations;
    }
    public double[] getHiddenRelationsKolom(int kolom) {
        double[] relatieKolom = new double[hiddenRelations.length];
        for (int i = 0; i < hiddenRelations.length; i++) {
            relatieKolom[i] = hiddenRelations[i][kolom];
        }
        return relatieKolom;
    }

    public void setHiddenRelations(double[][] hiddenRelations) {
        this.hiddenRelations = hiddenRelations;
    }
    
    public void setHiddenRelations(int positionRij, int positieKolom, double waarde) {
        this.hiddenRelations[positionRij][positieKolom] = waarde;
    }

    public double[][] getHiddenRelationsLastDelta() {
        return hiddenRelationsLastDelta;
    }

    public void setHiddenRelationsLastDelta(double[][] hiddenRelationsLastDelta) {
        this.hiddenRelationsLastDelta = hiddenRelationsLastDelta;
    }
    
    public void setHiddenRelationsLastDelta(int positionRij, int positionKolom, double waarde) {
        this.hiddenRelationsLastDelta[positionRij][positionKolom] = waarde;
    }
    
}

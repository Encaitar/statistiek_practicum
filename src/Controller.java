
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import mnist.tools.MnistManager;
import org.apache.log4j.Logger;

public class Controller {
    final int AANTALTRAININGEN = 3;
    final int OUTPUTSIZE = 10;
    final int HIDDENSIZE = 20; //hoe hoger hoe beter
    final double LEARNINGRATE = 0.25;
    final double MOMENTUM = 0.25;
    //grijswaarde voor bepaling input neuron 1 of 0
    final int OMSLAGWAARDE = 127;
    
    public static Logger logger = Logger.getLogger(Controller.class);
    private MnistManager mmTrainingSet;
    private MnistManager mmT10Set;
    public Data data;
    
    public Controller(MnistManager mmTrainingSet, MnistManager mmT10Set)
    {
        Utils.setOmslagWaarde(OMSLAGWAARDE);
        try {
            int[][] firstImage = mmTrainingSet.getImages().readImage();
            data = new Data(firstImage, HIDDENSIZE, OUTPUTSIZE);
            mmTrainingSet.getLabels().readLabel(); //Synchronize with images
            
            this.mmTrainingSet = mmTrainingSet;
            this.mmT10Set = mmT10Set;
            List<Integer> imagePositions = new ArrayList();
            for (int i = 1; i <= mmTrainingSet.getImages().getCount(); i++) {
                imagePositions.add(i);
            }
            
            for (int i = 0; i < AANTALTRAININGEN; i++) {
                if(i != 0) {Collections.shuffle(imagePositions);}
                logger.info("Training "+(i+1)+" gestart.");
                this.mmTrainingSet.setCurrent(1);
                startTrainingNN(imagePositions);
            }
            
            double percentage = checkPercentageGoodNN();
            
            logger.info("Percentage plaatjes herkent: "+percentage);
        } catch(Exception e) {
 
            logger.error("image inlezen mistlukt: ",e);
        }
        
    }
    
    private void startTrainingNN(List<Integer> imagePositions)
    {
        try {
            int[][] image;
            int begin = (int) mmTrainingSet.getImages().getCurrentIndex();
            for (int i = begin; i < mmTrainingSet.getImages().getCount(); i++) { //FOR EVERY IMAGE
                if(i %10000 == 0)
                {
                    logger.info("Trainer is nu bij afbeelding: "+i);
                }
                mmTrainingSet.setCurrent(imagePositions.get(i));
                image = mmTrainingSet.getImages().readImage(); //READ CURRENT IMAGE
                logger.debug("Created image:" +i);
                
                data.setInputNeurons( Utils.changeGrayValuesToBlackWhite((Utils.setTwoDimensionToSingleArray(image))) );
                dataSetHiddenNeuron(); //FOR EVERY HIDDEN NEURON --> inputneurons to netj
               
                //PRINT HIDDEN BEFORE
                String log = "";
                for (int j = 0; j < data.getHiddenNeurons().length; j++) {
                    log+=" - "+data.getHiddenNeurons()[j];                    
                }
                logger.debug("HIDDEN BEFORE SIGMOID: "+log);
                
                logger.debug("Setted Hiddenneurons");
                                
                data.hiddenNeuronsToSigmoid();
                logger.debug("Hidden neurons transformed with sigmoidfunction");
                
                //PRINT HIDDEN
                log = "";
                for (int j = 0; j < data.getHiddenNeurons().length; j++) {
                    log+=" - "+data.getHiddenNeurons()[j];            
                }
                logger.debug("HIDDEN NEURONS: "+log);
                //END PRINT HIDDEN
                
                
                dataSetActivationForOutputNeuron(); //FOR EVERY OUTPUT NEURON -> activation to the outputneuron from hiddenNeurons
                logger.debug("Setted outputneurons (including sigmoid function)");
                
                //PRINT OUTPUT TEST
                log = "";
                for (int j = 0; j < data.getOutput().length; j++) {
                    log+= data.getOutput()[j];                    
                }
                logger.debug("OUTPUT NEURONS: "+log);
                //END PRINT OUTPUT TEST
                
                int label = mmTrainingSet.getLabels().readLabel();
                for (int j = 0; j < data.getOutput().length; j++) {
                    double werkelijkeWaarde = data.getOutput()[j];
                    if(label == j) {
                        data.setErrorOutput(j, Utils.getOutputError(1.0, werkelijkeWaarde));
                    }
                    else {
                        data.setErrorOutput(j, Utils.getOutputError(0.0, werkelijkeWaarde));   
                    }
                }
                logger.debug("Setted error for the output neurons");
                
                for (int j = 0; j < data.getHiddenNeurons().length; j++) {
                    double hiddenNeuron = data.getHiddenNeurons()[j];
                    data.setErrorHiddenNeuron(j, Utils.getHiddenError(hiddenNeuron, data.getErrorOutput(), data.getHiddenRelationsKolom(j)));
                }
                logger.debug("Setted error for the hidden neurons");
                
                updateHiddenRelations();
                updateInputRelations();
                
                log = "";
                for (int j = 0; j < data.getHiddenRelations().length; j++) {
                    for (int k = 0; k < data.getHiddenRelations()[j].length; k++) {
                        log += data.getHiddenRelations()[j][k] + "  ";                        
                    }
                }
                logger.debug("Hidden relaties: "+log);
            }
        } catch(Exception e) {
            logger.error("Training data crash", e);
        }
    }
    
     private double checkPercentageGoodNN()
    {
         try {
            int[][] image;
            int begin = (int) mmT10Set.getImages().getCurrentIndex();
            int labelGelijkAanImage = 0;
            
            for (int i = begin; i < mmT10Set.getImages().getCount(); i++) { //FOR EVERY IMAGE
                
                image = mmT10Set.getImages().readImage(); //READ CURRENT IMAGE
                logger.debug("Created image:" +i);
                if(i %1000 == 0)
                {
                    logger.info("Is nu bij plaatje: "+i);
                }
                
                data.setInputNeurons( Utils.changeGrayValuesToBlackWhite((Utils.setTwoDimensionToSingleArray(image))) );
                
                dataSetHiddenNeuron();
                              
                
                //PRINT HIDDEN BEFORE
                  logger.debug("HIDDEN BEFORE SIGMOID: ");
                  for (int j = 0; j < data.getHiddenNeurons().length; j++) {
                  logger.debug(" | "+data.getHiddenNeurons()[j]);                    
                  }
                //END PRINT HIDDEN
                logger.debug("Setted Hiddenneurons");
                                
                data.hiddenNeuronsToSigmoid();
                logger.debug("Hidden neurons transformed with sigmoidfunction");
                 //PRINT HIDDEN
                
                String hiddenNeurons = "HIDDEN NEURONS: ";
                 for (int j = 0; j < data.getHiddenNeurons().length; j++) {
                     hiddenNeurons+= " - "+data.getHiddenNeurons()[j];            
                 }
                 logger.debug(hiddenNeurons);
                 //END PRINT HIDDEN
                dataSetActivationForOutputNeuron(); //FOR EVERY OUTPUT NEURON -> activation to the outputneuron from hiddenNeurons
                
               logger.debug("Setted outputneurons (including sigmoid function)");
                
                //PRINT OUTPUT TEST
                 String outputTest = "OUTPUT NEURONS: ";
                 for (int j = 0; j < data.getOutput().length; j++) {
                     outputTest += data.getOutput()[j];                    
                 }
                 logger.debug(outputTest);
                 //END PRINT OUTPUT TEST
                
                int label = mmT10Set.getLabels().readLabel();
                
                
                
                if(OutputIsLabel(label, data.getOutputInt()))
                {
                    labelGelijkAanImage++;
                    
                }
            }
            System.out.println("AantalgoedeImages= "+labelGelijkAanImage);
            
            double labelGelijkAanImageDouble = (double)labelGelijkAanImage;
            double imageLengthDouble = (double) mmT10Set.getImages().getCount();
         
            
            return ( labelGelijkAanImageDouble / imageLengthDouble) * 100.0;
            
         } catch(Exception e) {
            logger.error("CheckPercentageGoodNN crash: "+e);
        }
         
        return -1.0;
    }
    
     private boolean OutputIsLabel(int outputLabel, int[] outputTest)
     {
         boolean outputReturn = false;
         int sum = 0;
         int outputAan = -1;
         for (int i = 0; i < outputTest.length; i++) {
             sum = sum+outputTest[i];
             if(outputTest[i] == 1)
             {
                 outputAan = i;
             }
         }
         if(sum == 1 && outputAan == outputLabel)
         {
             outputReturn = true;
         }         
         return outputReturn;
     }
     
     private void updateHiddenRelations()
    {
        double[] outputError = data.getErrorOutput(); //lengte = 10
        double[] hiddenNeurons = data.getHiddenNeurons(); //50
        double[][] hiddenRelations = data.getHiddenRelations(); // lengte 10 breedte 50
        double[][] hiddenRelationsDelta = data.getHiddenRelationsLastDelta();
        
        for (int rij = 0; rij < hiddenRelations.length; rij++) {
            
            
            for (int kolom = 0; kolom < hiddenRelations[rij].length; kolom++) {
                
                double deltaGewicht = Utils.getDeltaGewicht(LEARNINGRATE, MOMENTUM, outputError[rij], hiddenRelationsDelta[rij][kolom], hiddenNeurons[kolom]);
                data.setHiddenRelationsLastDelta(rij, kolom, deltaGewicht);
                data.setHiddenRelations(rij, kolom, (deltaGewicht + data.getHiddenRelations()[rij][kolom]));
            }
        }
        
        
       logger.debug("setted new weights for hiddenRelations");
    }
    
    private void updateInputRelations()
    {
        double[] hiddenError = data.getErrorHiddenNeurons(); //lengte = 50
        int[] inputNeurons = data.getInputNeurons(); //768
        double[][] inputRelations = data.getInputRelations(); // lengte 50 breedte 768
        double[][] inputRelationsDelta = data.getInputRelationsLastDelta();
        
        for (int rij = 0; rij < inputRelations.length; rij++) {
            
            
            for (int kolom = 0; kolom < inputRelations[rij].length; kolom++) {
                
                double deltaGewicht = Utils.getDeltaGewicht(LEARNINGRATE, MOMENTUM, hiddenError[rij], inputRelationsDelta[rij][kolom], inputNeurons[kolom]);
                data.setInputRelationsLastDelta(rij, kolom, deltaGewicht);
                data.setInputRelations(rij, kolom, (deltaGewicht + data.getInputRelations()[rij][kolom]));
            }
        }
        
        logger.debug("setted new weights for inputRelations");
    }
          
    public void dataSetHiddenNeuron() {
        for (int j = 0; j < data.getHiddenNeurons().length; j++) { //FOR EVERY HIDDEN NEURON --> inputneurons to netj
           data.setHiddenNeuron(j, Utils.getNet(data.getInputNeurons(), data.getInputRelations()[j]));
        }
    }
    
    public void dataSetActivationForOutputNeuron() {
        for (int j = 0; j < data.getOutput().length; j++) { //FOR EVERY OUTPUT NEURON -> activation to the outputneuron from hiddenNeurons
            double net = Utils.getNet(data.getHiddenNeurons(), data.getHiddenRelations()[j]);
            logger.debug("NET OUTPUT: " + net);
            double sigmoid = Utils.sigMoid(net);
            logger.debug("SIGMOID OUTPUT: "+sigmoid);
            //data.setOutputNeuron(j, sigmoid);
            data.setOutput(j, sigmoid);
        }
    }
}
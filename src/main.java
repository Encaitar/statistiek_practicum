
import java.io.IOException;
import mnist.tools.MnistManager;

//Opdracht machine learning voor minor Rott Open Data
//
// 1. Programmeer een neuraal netwerk in Java bestaande uit input laag, hidden laag en output laag. 
// 2. Zorg ervoor dat de aantallen neuronen in elk vd lagen configureerbaar is.
// 3. Experimenteer met verschillende waarden voor de learning rate (eta) en momenten (alfa) en het aantal cellen in de hidden laag.
// 4. Train je netwerk met de training set (50000 bitmaps) en test met de testset (10000).
// 5. Maak een kort verslag waarin je je programma en de resultaten bespreekt. 
//
//Zie http://www.codeproject.com/KB/library/NeuralNetRecognition.aspx voor voorbeelden en data.
public class main {
   
    public static void main(String[] args) {
        try {
            String labelTrainFilePad = "files/train-labels.idx1-ubyte";
            String imageTrainFilePad = "files/train-images.idx3-ubyte";
            
            String labelT10kFilePad = "files/t10k-labels.idx1-ubyte";
            String imageT10kFilePad = "files/t10k-images.idx3-ubyte";
            
            MnistManager mmTrainingSet = new MnistManager(imageTrainFilePad, labelTrainFilePad);
            MnistManager mmT10Set = new MnistManager(imageT10kFilePad, labelT10kFilePad);
            
            Controller controller = new Controller(mmTrainingSet, mmT10Set);
            
        } catch (IOException e) {
            System.out.println("inlezen file mislukt: " + e);
        }
    }
}
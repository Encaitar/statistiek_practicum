public class Utils {
    public static int omslagWaarde;
    public static void setOmslagWaarde(int omslagwaardeSent)
    {
        Utils.omslagWaarde = omslagwaardeSent;
    }
    public static int[] changeGrayValuesToBlackWhite(int[] values)
    {
        for (int i = 0; i < values.length; i++) {
            values[i] = (values[i] > omslagWaarde) ? 1 : 0;            
        }
        
        return values;
    }
    
    public static double getNet(int[] inputNeurons, double[] weights) {
        double net = 0.0;
        for(int i = 0; i < weights.length; i++) {            
            net += weights[i] * inputNeurons[i];
        }
        
        return net;
    }
    
    public static double getNet(double[] hiddenNeurons, double[] weights) {
        double net = 0.0;
        for(int i = 0; i < weights.length; i++) {
            net += weights[i] * hiddenNeurons[i];
        }
        
        return net;
    }
    
    //bereken actual number
    public static double sigMoid(double net){
        return 1.0 / (1.0 + Math.exp(-1.0*net));
    }
    
    //bereken error van de output laag
    public static int getOutputError(int targetNr, int actualNr){
        return (targetNr-actualNr)*actualNr*(1-actualNr);
    }
    public static double getOutputError(double targetNr, double actualNr){
        return (targetNr-actualNr)*actualNr*(1.0-actualNr);
    }
    
    public static double getHiddenError(double hiddenActivatedNeuron,int[] outputError, double[] weightWK){
        double somOutput = 0.0;        
        for(int i = 0; i < weightWK.length; i++) {
            somOutput += ((double) outputError[i]) * weightWK[i];
        }
        
        return hiddenActivatedNeuron*(1.00-hiddenActivatedNeuron)*somOutput;
    }
    
     public static double getHiddenError(double hiddenActivatedNeuron,double[] outputError, double[] weightWK){
        double somOutput = 0.0;        
        for(int i = 0; i < weightWK.length; i++) {
            somOutput += outputError[i] * weightWK[i];
        }
        
        return hiddenActivatedNeuron*(1.00-hiddenActivatedNeuron)*somOutput;
    }
    
    public static int[] setTwoDimensionToSingleArray(int[][] twoDimensionArray) {
        int[] singleDimensionArray = null;
        if(twoDimensionArray != null && twoDimensionArray.length > 0)
        {
            singleDimensionArray = new int[twoDimensionArray.length*twoDimensionArray[0].length];
            int positie = 0;
            for (int i = 0; i < twoDimensionArray.length; i++) {
                for (int j = 0; j < twoDimensionArray[i].length; j++) {
                    singleDimensionArray[positie] = twoDimensionArray[i][j];
                    positie++;
                }
            }
        }
        return singleDimensionArray;
        
    }              
    
    public static double[][] fillRandommised(double[][] relation)
    {
        for (int i = 0; i < relation.length; i++) {
            for (int j = 0; j < relation[i].length; j++) {
                relation[i][j] = 0.1 * (Math.random() - 0.5);// random waarde tussen -0.05 en 0.05            
            }
        }        
        return relation;
    }
    public static double getDeltaGewicht(double learningRate, double momentum,  double error, double deltaVorigGewicht, double neuron)
    {
        return (learningRate * (error*neuron)) + (momentum * deltaVorigGewicht);
    }
}